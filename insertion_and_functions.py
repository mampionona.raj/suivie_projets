#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 09:05:46 2021

@author: m2646
"""

import requests
import os
from bs4 import BeautifulSoup
#from selenium import webdriver
import pandas as pd
#from datetime import datetime as dt
import numpy as np
import warnings
import regex as re
warnings.filterwarnings("ignore")

from sqlalchemy import create_engine




 
#URL = "http://bpo.smartone-group.com/smtupdate3/upload/"

params_db = {
"host" : "172.16.42.217",
"database" : "smartone",
"user" : "da",
"password" : "d4t4_an4lYs7",
"port" : "5432"
}     

engine = create_engine('postgresql://{}:{}@{}:{}/{}'.
format(params_db['user'],
params_db['password'],
params_db['host'],
params_db['port'],
params_db['database'])) 

#******************************Input date range*********************************
start_date = input("Please enter start date: ")
#print('==============the date entered is: %s ====================' %date)
end_date = input("Please enter end date: ")
#*******************************************************************************
datelist= pd.date_range(start_date, end_date).strftime('%Y-%m-%d')
#datelist = datelist.map(lambda x: x.strftime('%Y-%m-%d'))

url_dict = {
    'KRRISSH':[
               {"SMT":["http://bpo.smartone-group.com/smtupdate3/upload/"]},
               {"PCES":["http://bpo.smartone-group.com/pcesupdate/upload/"]},
               {"PCES INF":["http://bpo.smartone-group.com/pcesINF/upload/"]},
               {"PCES FORWARD":["http://bpo.smartone-group.com/pcesINF2/upload/"]}, #==OK
               {"BRAND MANUFACTURER":["http://bpo.smartone-group.com/brndup2/upload/"]}, #f
               {"BRANDMAN DEDUPLICATION":["http://bpo.smartone-group.com/brandmandddup/upload/"]}, #f
               {"BRV":["http://bpo.smartone-group.com/brv/upload/"]}, #f
               {"BRAND GAP":["http://bpo.smartone-group.com/brandgap/upload/"]}, #f
               {"ITEM CHECK":["http://bpo.smartone-group.com/cleanup/upload/"], #f
                "ITEM CHECK1":["http://bpo.smartone-group.com/itemabso/upload/"]} #
               ],
    'AINA':[
            {"ROOMS": ["http://bpo.smartone-group.com/ARroomsV9/upload/"]},
            {"SSDET":["http://bpo.smartone-group.com/SSD_Projet/upload/"]}
            
           ],
    
    }

##############################setting up client and projects info###############################
client_link_dict ={
        'client':[],
        'project':[],
        'link':[],
    }

#for the final result- the table summary
output = {
         'client': [], 
         'projet': [],
         'batchname': [],
         'link':[],
         'volume':[],
         "volume_non_dupliquee":[],
         'date_injection':[]
         #ADD FLAG REDO
         
    }

################filling up the client_project info for the final result dataframe#######################
for key,value in url_dict.items():
    #for key2, value2 in value:
    #    print (key2, value2)
    '''print("key %s ,value %s" %(key ,value))
    print("CLIENT: ", key)
    print("nombre de projets" ,len(value))
    print("\n")

    print("projet: ",value)
    '''
    
    
    for el in value:
        
        #print("----------------------")
        for projet, link in el.items():
            projet = projet
            link = ','.join(link)
            #------for debugging------
            #print("Projet: %s \n Link: %s \n "%(projet, link))
            #-------------------------
            
            #print("=================")    
            client_link_dict['client'].append(key)
            client_link_dict['project'].append(projet)
            client_link_dict['link'].append(link)
        
    #print("===============================")    
    


print('\n')    


list_client = client_link_dict['client']
list_project = client_link_dict['project']
list_link = client_link_dict['link'] #contains list of FA files for some projects and contains folders for some



##################################################################################################

############################### get the page by url ##############################################
def get_page(url):
    #-------used for debugging-----------
    print("\n__________________________________________________________________")
    print("*******************Opening get_page function**********************")
    #------------------------------------

    URL = url
    page = requests.get(URL)
    '''print("--------------------------url----------------------", URL)
    '''
    soup = BeautifulSoup(page.content, 'html.parser')
    tables = soup.find_all("table")
    #print("-------table in beautifoul soup--------", tables)
    table = tables[0]
    #print(table)
        
    tab_data = [[cell.text for cell in row.find_all(["th","td"])]
                                for row in table.find_all("tr")]
    df = pd.DataFrame(tab_data)
    df.columns = df.iloc[0,:]
    df.drop(index=0,inplace=True)
    print(df.columns) 
    df.dropna(inplace = True)
    print("------------------------df in get_page---------------------------", df)
    
    #-------used for debugging-----------
    print("__________________________________________________________________")
    print("*******************Closing get_page function**********************")
    print("\n")
    #------------------------------------

    return df


##################################################################################################

############################## transform each page into df #######################################
'''each df contains the list of files or folders in each column ['Name'] '''
def transform_page_to_df (client, project, url):
    #-------used for debugging-----------
    '''print("__________________________________________________________________")
    print("*************Opening transform_page_to_df function****************")'''
    #------------------------------------

    
    '''This gets all the FA and even FA REDOs  :'''

    if (project == "SMT" ) | (project == "PCES") | (project == "PCES INF" ) | (project == 'PCES FORWARD'):
        df = get_page(url)
        cleaned_df = df[df['Name'].str.contains("FA")] 
        print("==============cleaned_df=========\n",cleaned_df)

        
            
    else:
        '''For other projects we get the folder path'''
        df = get_page(url)
        cleaned_df = df[df['Name'].str.contains("FA/")] 
        print("==============cleaned_df========\n",cleaned_df)
        

    if (project == "ITEM CHECK1"):
        cleaned_df['Project']="ITEM CHECK"
        cleaned_df['Name']=cleaned_df['Name'].apply(lambda x: str(x))
        cleaned_df['Client']=client
        cleaned_df['Link']=url+cleaned_df['Name']

    
    else:
        cleaned_df['Name']=cleaned_df['Name'].apply(lambda x: str(x))
        cleaned_df['Client']=client
        cleaned_df['Project']=project
        cleaned_df['Link']=url+cleaned_df['Name']

    
    '''print("__________________________________________________________________")
    print("**************closing transform_page_to_df function***************")
    print("\n")'''

    return cleaned_df
#########################################################################


########################removing all the redos###########################
'''def remove_redos(df):
    return df[~df['Name'].str.contains("REDO")]
    '''
###################modify to datetime#################################

def transforming_last_modified_type(df):
    '''print("______________________________________________________________________________")
    print("******************transforming_last_modified_type function********************")
    print("\n")'''
    
    #print("transforming_last_modified_type function")
    output = df
    output['Last modified'] = output['Last modified'].apply(lambda x: pd.to_datetime(x)) 
    output['Last modified'] = output['Last modified'].dt.strftime("%Y-%m-%d")
    

    return output

#############################Creation of list containing all files or all folders ######################
def file_list_creation(df,date):
    '''print("--------------------------------------------------------------------------")
    print("****************opening file_list_creation function**********************")
    print('\n')'''
    return np.array( df[df['Last modified'] == date][["Name"]]) 


############################ Opening all the FA files ####################################################
def opening_each_file(cleaned_df, filelist,date, link):
    #---------for debugging------------
    '''print("______________________________________________________________________________________")
    print("******************************opening_each_file function******************************")
    print("\n")'''
    #----------------------------------
    result = {}
    output = pd.DataFrame()
    #filelist = np.array( df[df['Last modified'] == date][["Name"]])  #filelis
    
    
    #---------for debugging-----------
    #print("*****************************************************************")
    #print('Nbr of files to open =', len(filelist))
    #---------------------------------
    
    
    if (len(filelist)>0):
        for i in range(0,len(filelist)):
            print("++++++++++++++++++++++++++++++++++")
            #print("filelist: ", filelist[i][0])
            print("iteration %d ... \n file corresponding %s"% (i+1, filelist[i][0]))
            
            file_link = link+ filelist[i][0] 
            #"http://bpo.smartone-group.com/pcesINF/upload/"+filelist[i][0] #+link
            
            #-----used for debugging---------
            print("File Name: \n", filelist[i][0])
            print("File_link: \n",file_link)
            #--------------------------------
            print("++++++++++++++++++++++++++++++++++")
            
            try:
            # reading the file by xlrd
                print("--> Opening the excel file ...")
                df = pd.read_excel(file_link)
                
                directory = "../suivie_projects_batches/"+str(cleaned_df.Client.unique()[0])+"/"+str(cleaned_df.Project.unique()[0])+"/"+str(date)+"/"
                file = str(filelist[i][0])+".xlsx" # 'test.xlsx'
                
                if not os.path.exists(directory):
                    os.makedirs(directory)
                
                print (directory, file )
                df.to_excel(os.path.join(directory, file))
                
                #------for debugging--------
                print(df)
                print (len(df))
                print("--> Summary: \n")
                print ("file name: %s , content = %d , not duplicate = %d, date_integration= %s" %(filelist[i][0],len(df),len(df.drop_duplicates()), date))
                #---------------------------
                
                print ("Thanks for your Excel file\n")
            except: # if you find specific Exception types, use them here
                try:
                    # reading as CSV file
                    print("--> Opening the csv file ...")
                    df = pd.read_csv(file_link, sep=',')
                    
                    directory = "../suivie_projects_batches/"+str(cleaned_df.Client.unique()[0])+"/"+str(cleaned_df.Project.unique()[0])+"/"+str(date)+"/"
                    file = str(filelist[i][0])+".xlsx" # 'test.xlsx'
                    
                    if not os.path.exists(directory):
                        os.makedirs(directory)
                    
                    print (directory, file )
                    df.to_excel(os.path.join(directory, file))
                    
                  
                    print("--> Summary: \n")
                    print ("file name: %s , \n content = %d , \n not duplicate = %d, \n date_integration= %s" %(filelist[i][0],len(df),len(df.drop_duplicates()), date))
                    print ("thanks for your CSV file\n")
                    
                except: # if you find specific Exception types, use them here
                    print ("sorry, now way, give me some usable file.\n")
                
                #print('Not duplicate:' ,len(df.drop_duplicates()))
            
            
            result['Client'] = cleaned_df.Client.unique()[0]
            result["Project"]= cleaned_df.Project.unique()[0]
            result['file']=filelist[i][0]
            result['file_link']=file_link
            result['injection_date'] = date
            result['nb_of_entries'] = len(df)
            result['nb_of_entries_not_duplicated']=len(df.drop_duplicates())
            


            if any(re.findall(r'redo', filelist[i][0], re.IGNORECASE)):
           
                print ("REDO Found!",)
                result['redo']=1
            else:
                print ("REDO Not found!")
                result['redo']= 0
            
            print("Output " , result)
            
            print("*******************Dont forget to upload the file in postgres, only those without duplicates**************************")
            #df to inject
            
            
            output = output.append( result, ignore_index = True)
    else:
        print("No list found for this specific date = ", date)
    return output
        #client, projet, filelist[i][0], file_link, len(df), len(df.drop_duplicates()), date

############################# We need a folderlist ##########################
    
############################# if folders ,i.e for some projects, we open the FA/ folders , by using the same file_list_creation(df, date) function########################
def opening_each_folder(client, project , cleaned_df_folder, folderlist, date, link): #link eg: http://bpo.smartone-group.com/ARroomsV9/upload/
    #---------for debugging-----------------
    #print("______________________________________________________________________________________")
    #print("opening_each_folder function")
    
    #---------for debugging------------
    '''print("______________________________________________________________________________________")
    print("******************************opening_each_folder function******************************")
    print("\n")'''
    #----------------------------------
    result = {}
    output = pd.DataFrame()

    print("*****************************************************************")
    print('Nbr of folders to open =', len(folderlist))
    if (len(folderlist)>0):
        #we open each folder by the same function
        #print(" >>>>>>>> Parameter link value = ", link)
        url_folder = link+folderlist #eg. http://bpo.smartone-group.com/ARroomsV9/upload/SSDetP2TR_4390037_in_progress_FA/
        
        #-----used for debugging----
        print("--> Folderlist:\n", folderlist)
        #print("--> url_folder:\n", url_folder)
        
        ##----##
        for k in range(0, len(folderlist)):
            url_folder = link+folderlist[k][0]
            print("\n Folder number %d " %(k+1))
            print("--> Folder Name:  %s, \n associated link: %s \n" %(folderlist[k], url_folder))
            print('\n ')
            #print("--> url_folder:\n", url_folder[0][0])
            
           
            
            # this creates a df with column "Name" containing all folder_name: eg: SSDetP2TR_4390037_in_progress_FA/
            df_folder = get_page(url_folder) # e.g get_page(http://bpo.smartone-group.com/ARroomsV9/upload/SSDetP2TR_4390037_in_progress_FA/)
            
            #once open, we get the file inside with transform_page_to_df(client, project, url) function
            df_folder.reset_index(inplace=True) #transform it into indexed df
            
            #we need to take only the files containing the string CORRECTEUR in its name, case ignored. 
            
            df_folder = df_folder[df_folder['Name'].str.contains("correcteur", na=False, case=False)]
            
            #transforming_last_modified_type
            df_folder_filtered = transforming_last_modified_type(df_folder) # df_folder[df_folder['Last modified']==date]
            
          
    
            filelist= file_list_creation(df_folder_filtered, date)
            #print("file_list_per_folder", filelist)
            if (len(filelist)>0):
                for i in range(0,len(filelist)):
                    #-----used for debugging-----
                    print("\n....Checking all existing files in the folder number %d named %s ....\n" % ( k+1, folderlist[k][0]))
                    #print("filelist: ", filelist[i][0])
                    print("....iteration number %d , \n existing file %s \n" %( i+1, filelist[i][0]))
                    #----------------------------
                    
                    
                    file_link = url_folder+ filelist[i][0] 
                    print("\n~~~~~~~~~~ Folder Name %s  ... \n \t ~~~~~ Containing File %s " %(folderlist[k][0], filelist[i][0]))
                    print("\n~~~~~~~~~~ File_link: ",file_link)
                    
                    try:
                        # reading the file by xlrd
                        df = pd.read_excel(file_link)
                        
                        directory = "../suivie_projects_batches/"+str(client)+"/"+str(project)+"/"+str(date)+"/"
                        file = str(filelist[i][0])+".xlsx" # 'test.xlsx'
                
                        if not os.path.exists(directory):
                            os.makedirs(directory)
                        
                        print ("directory: ", directory, file )
                        df.to_excel(os.path.join(directory, file))
                                
 
                        
                        #print(df)
                        print("++++++++++++++++content of excel df in each folder +++++++++++++++", df)
                        print (len(df))
                        print ("file name: %s , \n content = %d , \n not duplicate = %d, \n date_integration= %s" %(filelist[i][0],len(df),len(df.drop_duplicates()), date))
                    
                        print ("Thanks for your Excel file\n")
                        
                    except: # if you find specific Exception types, use them here
                        try:
                            # reading as CSV file
                            df = pd.read_csv(file_link, sep=',')
                            
                            directory = "../suivie_projects_batches/"+str(client)+"/"+str(project)+"/"+str(date)+"/"
                            file = str(filelist[i][0])+".xlsx" # 'test.xlsx'
                    
                            if not os.path.exists(directory):
                                os.makedirs(directory)
                            
                            print ("directory =",directory, file )
                            df.to_excel(os.path.join(directory, file))
                            
                           
                            print("+++++++++++++++content of csv df in each folder +++++++++", df)
                            print ("file name: %s , content = %d , not duplicate = %d, date_integration= %s" %(filelist[i][0],len(df),len(df.drop_duplicates()), date))
                            print ("thanks for your CSV file \n")
                        except: # if you find specific Exception types, use them here
                            print ("sorry, now way, give me some usable file.\n")
                        
                        #print('Not duplicate:' ,len(df.drop_duplicates()))
    
                    
                
                    result['Client'] = client #cleaned_df_folder.Client.unique()[0]
                    result["Project"]= project #cleaned_df_folder.Project.unique()[0]
                    result['file']=filelist[i][0]
                    result['file_link']=file_link#[i][0]
                    result['injection_date'] = date
                    result['nb_of_entries'] = len(df)
                    result['nb_of_entries_not_duplicated']=len(df.drop_duplicates())
                    
        
        
                    if any(re.findall(r'redo', filelist[i][0], re.IGNORECASE)):
                   
                        print ("REDO Found!",)
                        result['redo']=1
                    else:
                        print ("REDO Not found!")
                        result['redo']= 0
                    
                    print("Output " , result)
                    
                    print("*******************Dont forget to upload the file in postgres, only those without duplicates**************************")
                    #df to inject
                
                    output = output.append( result, ignore_index = True)
            else:
                print("No file to open in this current folder...\n")
        
    
    else:
        print("No folders to open")
        


    return output #df_folder#df_folder_filtered 



summary_loop = pd.DataFrame()
'''for c, p, l, in zip(['KRRISSH','KRRISSH','KRRISSH','KRRISSH'],['SMT','PCES','PCES INF', 'PCES FORWARD'],["http://bpo.smartone-group.com/smtupdate3/upload/","http://bpo.smartone-group.com/pcesupdate/upload/","http://bpo.smartone-group.com/pcesINF/upload/","http://bpo.smartone-group.com/pcesINF2/upload/"]):
    print("***************************************************")
    print("client %s with project %s and upload link %s" %(c,p,l))
    test = transform_page_to_df(c, p, l)
    test = transforming_last_modified_type(test)
    print('test df for each loop==== ' , test)
    filelist = file_list_creation(test, date)
    
    df_out = opening_each_file(test, filelist, date, l)
    print("DF OUT $$$$$$$$$$$$$$$$$$ ", df_out)
    #if project = x ...:
        
    summary_loop = summary_loop.append(df_out)'''
      

for date in datelist:
    print ("date: ", date)
    for c, p, l, in zip(list_client,list_project,list_link):
    #for c, p, l, in zip(["KRRISSH"],["ITEM CHECK"],["http://bpo.smartone-group.com/cleanup/upload/"]):
    
        print("***************************************************")
        print("client %s with project %s and upload link %s" %(c,p,l))
        test = transform_page_to_df(c, p, l)
        test = transforming_last_modified_type(test)
        #print('test df for each loop==== \n' , test)
        #test = test[test['Last modified']== date] 
        
        folderlist = file_list_creation(test, date)
        
        if (p == 'PCES') | (p== "SMT") | (p== "PCES INF") | (p == "PCES FORWARD"):
            df_out = opening_each_file(test, folderlist, date, l)#(test, filelist, date, l)
        else:
            df_out = opening_each_folder(c, p , test, folderlist, date, l)#(test, filelist, date, l)
            
        print( "================DF_OUT ====================", df_out)
            
        summary_loop = summary_loop.append(df_out, ignore_index=True)
    
table_name= 'suivie_injection'




summary_loop.to_sql(table_name, con=engine, index=False, if_exists='append')
